<%-- 
    Document   : index
    Created on : 08-jul-2015, 10:12:44
    Author     : USUARIO
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="sisventas.Interface"%>
<%@page import="java.rmi.Naming"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Scrolling Nav - Start Bootstrap Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/scrolling-nav.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<!-- The #page-top ID is part of the scrolling feature - the data-spy and data-target are part of the built-in Bootstrap scrollspy function -->

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">Producto</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a class="page-scroll" href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#about">Agregar producto</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="ventas.jsp">Ventas</a>
                    </li>                    

                </ul>
              
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Intro Section -->
    <section id="intro" class="intro-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <table width="100%" style="text-align:center;" class= "table table-bordered table-hover" >
                        <thead>
                        <tr>
                            <td><strong>ID</strong></td>
                            <td><strong>Nombre</strong></td>
                            <td><strong>Precio</strong></td>
                            <td><strong>Stock</strong></td>
                            <td><strong>Accion</strong></td>
                        </tr>
                        </thead>
                        <tbody>
                        <%
                        Interface i= (Interface)Naming.lookup("//localhost:9050/servi1"); 
                        ArrayList <String[]> res = new ArrayList<String[]>();
                        res = i.mostrar("productos");
                        //out.print(res.get(0)[0]);
                        for (int ii = 0; ii < res.size(); ii++) { %>
                        <tr>
                            <td><%out.print(res.get(ii)[0]);%></td>
                            <td><%out.print(res.get(ii)[1]);%></td>
                            <td><%out.print(res.get(ii)[2]);%></td>
                            <td><%out.print(res.get(ii)[3]);%></td>
                            <td>
                                <button type="button" class="btn btn btn-info" onclick="actualizar(<%out.print(res.get(ii)[0]);%>)" data-toggle="modal" data-target="#myModal">Editar</button> 
                                <button type="button" class="btn btn-danger"  onclick="eliminar(<%out.print(res.get(ii)[0]);%>)">Eliminar</button>
                            </td>
                        </tr>
                        <%                                
                                //out.print("<br>");
                            }
                        %>                            

                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </section>

    <!-- About Section -->
    <section id="about" class="about-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                    <center>
                        <strong><h2>Agregar Producto</h2></strong><br>
                        <form action="guardar.jsp" method="POST">
                        <table>
                            <tr >
                                <td>Nombre:</td>
                                <td><input type="text" name="producto"></td>
                            </tr>
                            <tr>
                                <td>Precio:</td>
                                <td><input type="text" name="precio"></td>
                            </tr>
                            <tr>
                                <td>Stock</td>
                                <td><input type="text" name="stock"></td>
                            </tr>
                        </table>
                        <br>
                        <button type="submit" class="btn btn btn-info">Agregar</button>
                        </form>
          </center>
                </div>
            </div>
        </div>
    </section>

    <!-- Services Section -->
    <section id="services" class="services-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1>Services Section</h1>
                </div>
            </div>
        </div>
    </section>

    <!-- Contact Section -->
    <section id="contact" class="contact-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1>Contact Section</h1>
                </div>
            </div>
        </div>
    </section>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Scrolling Nav JavaScript -->
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/scrolling-nav.js"></script>
    <script src="js/sisventas.js"></script>
    
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Editar Producto</h4>
      </div>
      <div class="modal-body prod">

           
      </div>
      <div class="modal-footer">

      </div>
    </div>
  </div>
</div>

</body>

</html>

