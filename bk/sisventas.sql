/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50543
Source Host           : localhost:3306
Source Database       : sisventas

Target Server Type    : MYSQL
Target Server Version : 50543
File Encoding         : 65001

Date: 2015-07-09 10:17:01
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for productos
-- ----------------------------
DROP TABLE IF EXISTS `productos`;
CREATE TABLE `productos` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `producto` varchar(255) NOT NULL,
  `precio` double(255,1) NOT NULL,
  `stock` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `pk` FOREIGN KEY (`id`) REFERENCES `ventas` (`idproducto`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of productos
-- ----------------------------

-- ----------------------------
-- Table structure for ventas
-- ----------------------------
DROP TABLE IF EXISTS `ventas`;
CREATE TABLE `ventas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cantidad` varchar(255) DEFAULT NULL,
  `idproducto` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idproducto` (`idproducto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ventas
-- ----------------------------
