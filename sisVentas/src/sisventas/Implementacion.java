
package sisventas;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * @author anthonywainer
 */
public class Implementacion extends UnicastRemoteObject implements Interface {
    
    public Implementacion() throws RemoteException{
        super();
    }

    @Override
    public ArrayList<String[]> mostrar(String ta) throws RemoteException {
    try {
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        System.out.println("Registro exitoso");
        Connection con = null;
        con = DriverManager.getConnection(
                    "jdbc:mysql://localhost/sisventas?"
                    + "user=root&password=123");
        ResultSet rs = null;
        Statement cmd = null;
        cmd = con.createStatement();         
        rs = cmd.executeQuery("SELECT * FROM "+ta); 
        ArrayList <String[]> res = new ArrayList<String[]>();
        int columnCount = rs.getMetaData().getColumnCount();
        
        while (rs.next()) {
            String[] row = new String[columnCount];
            for (int i=0; i <columnCount ; i++)
            {
               row[i] = rs.getString(i + 1);
            }
            res.add(row);            
        
        }
        System.out.println( res.get(0)[0] );
        rs.close();  
        return res;
    } catch (Exception e) {
        System.out.println(e.toString());
    }  
       return null;
    
    }

    @Override
    public Integer eliminar(String ta, int id) throws RemoteException {
    try {
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        System.out.println("Registro exitoso");
        Connection con = null;
        con = DriverManager.getConnection(
                    "jdbc:mysql://localhost/sisventas?"
                    + "user=root&password=123");
        ResultSet rs = null;
        Statement cmd = null;
        cmd = con.createStatement();         
        cmd.executeUpdate("DELETE FROM "+ta+" WHERE id = "+id);

        } catch (Exception ee) {
            System.out.println(ee.toString());
        }  
       return null;
    
    }

    @Override
    public Integer actualizar(String ta, int id, String a) throws RemoteException {
    try {
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        System.out.println("Registro exitoso");
        Connection con = null;
        con = DriverManager.getConnection(
                    "jdbc:mysql://localhost/sisventas?"
                    + "user=root&password=123");
        ResultSet rs = null;
        Statement cmd = null;
        cmd = con.createStatement();      
        System.out.println(a);
        cmd.executeUpdate("UPDATE  "+ta+" SET "+a+" WHERE id = "+id);

        } catch (Exception ee) {
            System.out.println(ee.toString());
        }  
       return null;   
    }

    @Override
    public ArrayList<String[]> veractualizar(String ta, int id) throws RemoteException {
    try {
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        System.out.println("Registro exitoso");
        Connection con = null;
        con = DriverManager.getConnection(
                    "jdbc:mysql://localhost/sisventas?"
                    + "user=root&password=123");
        ResultSet rs = null;
        Statement cmd = null;
        cmd = con.createStatement();         
        rs = cmd.executeQuery("SELECT * FROM "+ta+ " WHERE id= "+id); 
        ArrayList <String[]> res1 = new ArrayList<String[]>();
        int columnCount = rs.getMetaData().getColumnCount();
        
        while (rs.next()) {
            String[] row = new String[columnCount];
            for (int i=0; i <columnCount ; i++)
            {
               row[i] = rs.getString(i + 1);
            }
            res1.add(row);            
        
        }
        System.out.println( res1.get(0)[0] );
        rs.close();  
        return res1;
    } catch (Exception e) {
        System.out.println(e.toString());
    }  
       return null;
    }

    @Override
    public Integer agregar(String ta, String a) throws RemoteException {
    try {
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        System.out.println("Registro exitoso");
        Connection con = null;
        con = DriverManager.getConnection(
                    "jdbc:mysql://localhost/sisventas?"
                    + "user=root&password=123");
        ResultSet rs = null;
        Statement cmd = null;
        cmd = con.createStatement();
        
        cmd.executeUpdate("INSERT INTO  "+ta+" VALUES ( "+a+")");

        } catch (Exception ee) {
            System.out.println(ee.toString());
        }  
       return null;   
    }
}
