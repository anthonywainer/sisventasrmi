package sisventas;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * @author anthonywainer
 */
public interface Interface  extends Remote{
        public ArrayList <String[]> mostrar(String ta)throws RemoteException;
        public Integer agregar(String ta, String a )throws RemoteException;
        public Integer eliminar(String ta, int id)throws RemoteException;
        public Integer actualizar(String ta, int id, String a)throws RemoteException;
        public ArrayList <String[]> veractualizar(String ta, int id)throws RemoteException;
        
}
 